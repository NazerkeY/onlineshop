import java.io.IOException;
import java.sql.SQLException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Launch {
    private static final String regexForEmail = "^[A-Za-z0-9+_.-]+@[A-Za-z0-9.-]+$";
    private static final String regexForPassword = "^[A-Za-z0-9+_.$@#%-].{4,30}";

    public void launch() throws SQLException, IOException, ClassNotFoundException {
        Shop shop = new Shop();
        User admin = new Admin("admin@admin.com", "admin", "Admin", "Admin");
        System.out.println("Welcome to online shop! Please 'Sign in' to make any purchases.\n" +
                "If you don't have an account enter 'Register' to create new one.\n" +
                "Press 'Q' to exit.");
        Shop.connection();
        shop.createProductTable();
        shop.createUserTable();
        shop.createCartTable();

        boolean logged = false;
        boolean adminLogged = false;
        boolean processEmail = true;
        boolean processPassword = true;
        User curUser = new Customer();
        String curEmail = "";

        while(true){
            Scanner scanner = new Scanner(System.in);
            String option = scanner.nextLine();
            if(!option.equals("Q")){

                if(option.equals("Sign in") && !logged){
                    System.out.println("Enter your email:");
                    String email = scanner.nextLine();
                    System.out.println("Enter your password:");
                    String password = scanner.nextLine();

                    if(email.equals(admin.getEmail()) && password.equals(admin.getPassword())){
                        System.out.println("Welcome admin!\n" +
                                "If you want to insert a new product, enter 'Insert'\n" +
                                "If you want to extract a new product, enter 'Extract'\n ");
                        adminLogged = true;
                    }
                    else if(shop.checkUser(email, password)){
                        logged = true;
                        curEmail = email;

                        curUser.setEmail(email);
                        curUser.setPassword(password);
                        curUser.setFirstName(shop.getNameOfUser(email, password));

                        System.out.println(String.format("Welcome %s! If you want to exit, press 'Q'.\n" +
                                "If you want to get your info, enter 'Info'\n" +
                                "If you want to view products, enter 'View'\n" +
                                "If you want to view your cart, enter 'Cart'\n" +
                                "If you want to buy product, enter 'Add'\n" +
                                "If you want to remove product from your cart, enter 'Remove'\n" +
                                "If you want to make payment, enter 'Pay'\n" +
                                "If you want to top up balance, enter 'Top up'\n" +
                                "If you want to delete your account, enter 'Delete'", shop.getNameOfUser(email, password)));
                    }
                    else{
                        System.out.println("Your email or password does NOT match");
                        logged = false;
                        adminLogged = false;
                    }
                }
                else if(option.equals("Register")){
                    System.out.println("Enter your information to sign up");
                    while (processEmail){
                        System.out.println("Enter email:");
                        String email = scanner.nextLine();
                        Pattern patternEmail = Pattern.compile(regexForEmail);
                        Matcher matcherEmail = patternEmail.matcher(email);

                        if(matcherEmail.matches()){
                            while(processPassword){
                                System.out.println("Create your own password:");
                                String password = scanner.nextLine();
                                Pattern patternPassword = Pattern.compile(regexForPassword);
                                Matcher matcherPassword = patternPassword.matcher(password);

                                if(matcherPassword.matches()){
                                    System.out.println("Enter your name");
                                    String firstName = scanner.nextLine();

                                    System.out.println("Enter your surname");
                                    String lastName = scanner.nextLine();

                                    if(shop.checkUser(email, password)){
                                        System.out.println("Account exists");
                                        processPassword = false;
                                        processEmail = false;
                                    }
                                    else{
                                        shop.addUser(new Customer(email, password, firstName, lastName));

                                        curUser.setEmail(email);
                                        curUser.setPassword(password);
                                        curUser.setFirstName(shop.getNameOfUser(email, password));
                                        curUser.setLastName(lastName);

                                        System.out.println(String.format("You've been registered successfully, %s !\n" +
                                                "If you want to get your info, enter 'Info'\n" +
                                                "If you want to view products, enter 'View'\n" +
                                                "If you want to view your cart, enter 'Cart'\n" +
                                                "If you want to buy product, enter 'Add'\n" +
                                                "If you want to remove product from your cart, enter 'Remove'\n" +
                                                "If you want to make payment, enter 'Pay'\n" +
                                                "If you want to top up balance, enter 'Top up'\n" +
                                                "If you want to delete your account, enter 'Delete'", shop.getNameOfUser(email, password)));
                                        curEmail = email;
                                        logged = true;
                                        processPassword = false;
                                        processEmail = false;
                                    }
                                }
                                else{
                                    System.out.println("Your password length must be at least 5 characters");
                                }
                            }
                        }
                        else{
                            System.out.println("Invalid form for email");
                        }
                    }
                }

                else if(option.equals("Info") && logged){
                    if(shop.checkUser(curUser.getEmail(), curUser.getPassword())) {
                        shop.getInfo((Customer) curUser);
                    }
                    else{
                        System.out.println("No matches.");
                    }
                }

                else if(option.equals("Delete") && logged) {
                    System.out.println("Please enter your email to confirm  the process");
                    String emailToDelete = scanner.nextLine();
                    if (emailToDelete.equals("Q")) {
                        break;
                    } else {
                        if (curEmail.equals(emailToDelete)) {
                            shop.deleteUser(emailToDelete);
                            System.out.println("Your account was deleted");
                            break;
                        } else {
                            System.out.println("Email doesn't match.");
                        }
                    }
                }
                else if(option.equals("Cart") && logged){
                    System.out.print("Your cart: \n" );
                   ((Customer) curUser).getCartInfo((Customer) curUser);
                }

                else if(option.equals("Add") && logged){
                    System.out.println(shop.viewProducts());
                    System.out.println("Enter id of product that you want to add into cart:");
                    int id = scanner.nextInt();

                    if(curUser instanceof Customer){
                        ((Customer) curUser).addProductToCart((Customer) curUser, id);
                        System.out.print("Your cart: \n");
                        ((Customer) curUser).getCartInfo((Customer) curUser);
                    }
                    else{
                        System.out.println("You can't add any product into your cart.");
                    }
                }

                else if(option.equals("Remove") && logged){
                    if(curUser instanceof Customer){
                        if(((Customer) curUser).getCartInfo((Customer) curUser)){
                            System.out.println("Your cart is empty!");
                        }
                        else{
                            System.out.print("Your cart: \n");
                            ((Customer) curUser).getCartInfo((Customer) curUser);

                            System.out.println("Enter id of product that you want to remove from cart");
                            int id = scanner.nextInt();
                            ((Customer) curUser).removeProductFromCart((Customer) curUser, id);

                            System.out.print("Your cart: ");
                            ((Customer) curUser).getCartInfo((Customer) curUser);
                        }
                    }
                }

                else if(option.equals("Pay") && logged){
                    if(curUser instanceof Customer){
                            if(((Customer) curUser).getUserBalance((Customer) curUser) == 0){
                                System.out.println("Your balance is 0.");
                            }
                            else if(((Customer) curUser).getUserBalance((Customer) curUser) < ((Customer) curUser).getTotalSumToPay((Customer) curUser)){
                                System.out.println("Your balance is:\n " + ((Customer) curUser).getUserBalance((Customer) curUser) + "\n");
                                System.out.println("You don't have enough money");
                            }
                            else{
                                ((Customer) curUser).pay((Customer) curUser);
                                shop.updProductsCntSubtr(((Customer) curUser).getProductIdInCart((Customer) curUser));
                                System.out.println("You bought!");
                                shop.clearUserCart((Customer) curUser);
                                ((Customer) curUser).getCartInfo((Customer) curUser);
                                System.out.println("Now your balance is: " + ((Customer) curUser).getUserBalance((Customer) curUser) + "\n");
                            }
                        }
                    }

                else if(option.equals("Top up") && logged){
                    System.out.println("Enter the sum that you want to add (Limit 100 000tg)");
                    int sum = scanner.nextInt();

                    shop.updateUserBalanceAdd((Customer) curUser, sum);
                    System.out.println("Your balance is " + ((Customer) curUser).getUserBalance((Customer) curUser));
                }

                else if(option.equals("Sign in") && (logged || adminLogged)){
                    System.out.println("You've already logged in");
                }
                else if(option.equals("View") && (logged || adminLogged)){
                    System.out.println(shop.viewProducts());
                }
                else if(option.equals("Insert") && adminLogged){
                    System.out.println("Enter the product name:");
                    String productName = scanner.nextLine();

                    System.out.println("Enter price:");
                    int price = scanner.nextInt();

                    System.out.println("Enter count:");
                    int count = scanner.nextInt();

                    if((price > 0 && price < 1000000) && (count >= 0 && count < 10000)){
                        if(admin instanceof Admin){
                            ((Admin) admin).insertProduct(new Product(productName, price, count));
                            System.out.println("Product was added.");
                        }
                        else{
                            System.out.println("You don't have any permission!");
                        }
                    }
                    else{
                        System.out.println("Price or Count value is not possible!");
                    }
                }

                else if(option.equals("Extract") && adminLogged){
                    System.out.println(shop.viewProducts());
                    System.out.println("Enter the product id that you want to remove:");

                    int productId = scanner.nextInt();
                    if(admin instanceof Admin){
                        ((Admin) admin).deleteProduct(productId);
                        System.out.println("Product was removed");
                    }
                    else{
                        System.out.println("You don't have any permission");
                    }
                }

                else if(option.equals("Q")){
                    break;
                }
            }
            else{
                break;
            }
        }
    }
}
