import java.io.IOException;
import java.sql.*;

public class Admin extends User{
    private final String type = "admin";
    public Admin(String email, String password, String firstName, String lastName) {
        super(email, password, firstName, lastName);
    }
    public String getType(){
        return type;
    }

    public long insertProduct(Product product) throws SQLException, IOException, ClassNotFoundException {
        long productId = 0;
        String sqlStatement = "INSERT INTO products(product_name, price, count) VALUES(?, ?, ?)";
        try(Connection connection = Shop.connection();
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement, Statement.RETURN_GENERATED_KEYS)){

            preparedStatement.setString(1, product.getName());
            preparedStatement.setInt(2, product.getPrice());
            preparedStatement.setInt(3, product.getCount());

            int affectedRows = preparedStatement.executeUpdate();
            if(affectedRows > 0){
                try(ResultSet resultSet = preparedStatement.getGeneratedKeys()){
                    if(resultSet.next()){
                        productId = resultSet.getLong(1);
                    }
                }
            }
        }
        return productId;
    }

    public void deleteProduct(int productId){
        String sqlStatement = "DELETE FROM products WHERE id = ?";
        try(Connection connection = Shop.connection();
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)){
            preparedStatement.setInt(1, productId);
            preparedStatement.executeUpdate();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException exception) {
            exception.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
