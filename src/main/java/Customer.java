import org.jasypt.util.password.StrongPasswordEncryptor;

import java.io.IOException;
import java.sql.*;

public class Customer extends User {
    private final String type = "customer";
    private int balance;

    public Customer(){
        super();
    }

    public Customer(String email, String password, String firstName, String lastName) {
        super(email, password, firstName, lastName);
    }
    public String getType(){
        return type;
    }
    public int getBalance(){
        return balance;
    }
    public void setBalance(int balance){
        this.balance = balance;
    }


    @Override
    public String toString() {
        return super.toString() + "Customer{" +
                "balance=" + balance +
                '}';
    }

    public void removeProductFromCart(Customer user, int productId){
        String sqlStatement = "DELETE FROM cart WHERE product_cart_id = ? AND user_cart_id = ?";
        try(Connection connection = Shop.connection();
        PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)){
            preparedStatement.setInt(1, productId);
            preparedStatement.setInt(2, user.getUserId(user.getEmail(), user.getPassword()));
            preparedStatement.executeUpdate();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException exception) {
            exception.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public int addProductToCart(Customer user, int productId){
        String sqlStatement = "INSERT INTO cart(product_cart_id, user_cart_id) VALUES(?, ?)";
        int id = 0;

        try(Connection connection = Shop.connection();
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement, Statement.RETURN_GENERATED_KEYS)){
            preparedStatement.setInt(1, productId);
            preparedStatement.setInt(2, getUserId(user.getEmail(), user.getPassword()));

            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows > 0) {
                try (ResultSet resultSet = preparedStatement.getGeneratedKeys()) {
                    if (resultSet.next()) {
                        id = resultSet.getInt(1);
                    }
                }
            }
            else{
                System.out.println("No such product!");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException exception) {
            exception.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return id;
    }


    public int getProductIdInCart(Customer user){
        int productIdInCart = 0;
        String sqlStatement = "SELECT product_cart_id FROM cart WHERE user_cart_id = ?";
        try(Connection connection = Shop.connection();
        PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)){
            preparedStatement.setInt(1, getUserId(user.getEmail(), user.getPassword()));
            try(ResultSet resultSet = preparedStatement.executeQuery()){
                while(resultSet.next()){
                    productIdInCart = resultSet.getInt("product_cart_id");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException exception) {
            exception.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return productIdInCart;
    }

    public boolean getCartInfo(Customer user){

        boolean isEmpty = false;
        String sqlStatement = "SELECT products.id, products.product_name, products.price, users.user_id,  " +
                "COUNT(*) as cur_count, (products.price * COUNT(*)) AS totalPrice FROM products " +
                "FULL JOIN cart ON products.id = cart.product_cart_id " +
                "INNER JOIN users ON users.user_id = cart.user_cart_id " +
                "WHERE users.user_id = ? " +
                "GROUP BY products.id, users.user_id, cart.count";

        try(Connection connection = Shop.connection();
        PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)){
           preparedStatement.setInt(1, user.getUserId(user.getEmail(), user.getPassword()));
            try(ResultSet resultSet = preparedStatement.executeQuery()){
                if(!resultSet.next()){
                   isEmpty = true;
                }
                else {
                    do {
                        int productId = resultSet.getInt("id");
                        String productName = resultSet.getString("product_name");
                        int productPrice = resultSet.getInt("price");
                        int count = resultSet.getInt("cur_count");
                        int totalPrice = resultSet.getInt("totalPrice");

                        System.out.println("Product{" +
                                "id=" + productId +
                                ", productName='" + productName + '\'' +
                                ", price=" + productPrice +
                                ", count=" + count +
                                ", total price=" + totalPrice +
                                '}');
                        isEmpty = false;
                    }  while (resultSet.next());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException exception) {
            exception.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return isEmpty;
    }

    public void pay(Customer user) {
        String sqlStatement = "UPDATE users SET balance = ? WHERE user_id = ?";
        try (Connection connection = Shop.connection();
             PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, (user.getUserBalance(user) - getTotalSumToPay(user)));
            preparedStatement.setInt(2, getUserId(user.getEmail(), user.getPassword()));
            preparedStatement.executeUpdate();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException exception) {
            exception.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public int getTotalSumToPay(Customer user){
        int totalSumToPay = 0;

        String sqlStatement = "SELECT (products.price * COUNT(*)) AS total_sum FROM cart LEFT JOIN products ON products.id = cart.product_cart_id " +
                "WHERE cart.user_cart_id = ? " +
                "GROUP BY cart.count, products.price";
        try(Connection connection = Shop.connection();
        PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)){
            preparedStatement.setInt(1, getUserId(user.getEmail(), user.getPassword()));
            try(ResultSet resultSet = preparedStatement.executeQuery()){
                while (resultSet.next()){
                    totalSumToPay = resultSet.getInt("total_sum");
                }
            } catch (SQLException exception) {
                exception.printStackTrace();
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return totalSumToPay;
    }

    public int getUserBalance(Customer user){
        int balance = 0;
        String sqlStatement = "SELECT balance FROM users WHERE user_id = ?";
        try(Connection connection = Shop.connection();
        PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)){
            preparedStatement.setInt(1, getUserId(user.getEmail(), user.getPassword()));
            try(ResultSet resultSet = preparedStatement.executeQuery()){
                while(resultSet.next()){
                    balance = resultSet.getInt("balance");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException exception) {
            exception.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return balance;
    }

    public int getUserId(String passEmail, String passPassword){
        String sqlStatement = "SELECT user_id, password FROM users WHERE email = ?";
        int id = 0;
        try(Connection connection = Shop.connection();
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setString(1, passEmail);
            try(ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    String encryptedStoredPassword = resultSet.getString("password");
                    StrongPasswordEncryptor encryptor = new StrongPasswordEncryptor(); //to encrypt the password that user entered

                    if(encryptor.checkPassword(passPassword, encryptedStoredPassword)){
                        id = resultSet.getInt("user_id");
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException exception) {
            exception.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return id;
    }
}
