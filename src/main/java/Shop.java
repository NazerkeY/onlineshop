import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.properties.EncryptableProperties;
import org.jasypt.util.password.StrongPasswordEncryptor;

import java.io.*;
import java.sql.*;
import java.util.*;

public class Shop {

    public static Connection connection() throws ClassNotFoundException, IOException, SQLException {
        Class.forName("org.postgresql.Driver");

        StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        encryptor.setPassword("jasypt");

        Properties properties = new EncryptableProperties(encryptor);
        InputStream inputStream = new FileInputStream("src/main/resources/config.properties");
        properties.load(inputStream);

        return DriverManager.getConnection(properties.getProperty("url"), properties.getProperty("user"), properties.getProperty("password"));
    }


    public void createCartTable(){
        String sqlStatement = "CREATE TABLE IF NOT EXISTS cart(product_cart_id INT, user_cart_id INT, count INT, " +
                "totalPrice INT, FOREIGN KEY(product_cart_id) REFERENCES products(id)," +
                "FOREIGN KEY(user_cart_id) REFERENCES users(user_id))";
        try(Connection connection = connection();
            Statement statement = connection.createStatement()){
            statement.executeUpdate(sqlStatement);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException exception) {
            exception.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void createUserTable(){
        String sqlStatement ="CREATE TABLE IF NOT EXISTS users(user_id SERIAL PRIMARY KEY, " +
                "email VARCHAR(120) UNIQUE NOT NULL, password VARCHAR(120) NOT NULL, first_name VARCHAR(120), " +
                "last_name VARCHAR(120), balance INT,type VARCHAR(120))";
        try(Connection connection = connection();
        Statement statement = connection.createStatement()){
            statement.executeUpdate(sqlStatement);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException exception) {
            exception.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void createProductTable(){
        String sqlStatement = "CREATE TABLE IF NOT EXISTS products(id SERIAL PRIMARY KEY, " +
                "product_name VARCHAR(120) NOT NULL, price int NOT NULL, count int)";
        try(Connection connection = connection();
        Statement statement = connection.createStatement()){
            statement.executeUpdate(sqlStatement);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException exception) {
            exception.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void getInfo(Customer user){
        String sqlStatement = "SELECT email, first_name, last_name, balance FROM users WHERE email = ? ";
        try(Connection connection = connection();
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)){
            preparedStatement.setString(1, user.getEmail());
            try(ResultSet resultSet = preparedStatement.executeQuery()){
                while(resultSet.next()) {
                        String email = resultSet.getString("email");
                        String firstName = resultSet.getString("first_name");
                        String lastName = resultSet.getString("last_name");
                        int balance = resultSet.getInt("balance");

                        System.out.println("User: \n" +
                                "first name = " + firstName +
                                ", last name = " + lastName +
                                ", email = " + email +
                                ", balance = " + balance);
                    }
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public String viewProducts(){
        String sqlStatement = "SELECT * FROM products";
        ArrayList<Object> products = new ArrayList<>();
        try(Connection connection = connection();
        PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)){
            try(ResultSet resultSet = preparedStatement.executeQuery()){
                while (resultSet.next()){
                   int productId = resultSet.getInt("id");
                   String productName = resultSet.getString("product_name");
                   int price = resultSet.getInt("price");
                   int count = resultSet.getInt("count");

                   Product product = new Product(productId, productName, price, count);

                    products.add(product);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException exception) {
            exception.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return products.toString();
    }

    public void clearUserCart(Customer user){
        String sqlStatement = "DELETE FROM cart WHERE user_cart_id = ?";
        try(Connection connection = connection();
        PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)){
            preparedStatement.setInt(1, user.getUserId(user.getEmail(), user.getPassword()));
            preparedStatement.executeUpdate();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException exception) {
            exception.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void updateUserBalanceAdd(Customer user, int sum){
        String sqlStatement = "UPDATE users SET balance = ? WHERE user_id = ?";
        try(Connection connection = connection();
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)){
            preparedStatement.setInt(1, ((user.getUserBalance(user)) + sum));
            preparedStatement.setInt(2, user.getUserId(user.getEmail(), user.getPassword()));
            preparedStatement.executeUpdate();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException exception) {
            exception.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


    public void updProductsCntSubtr(int passProductId){
        String sqlStatement = "UPDATE products SET count = ? WHERE id = ?";
        try(Connection connection = connection();
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)){
            preparedStatement.setInt(1, getCount(passProductId) - 1);
            preparedStatement.setInt(2, passProductId);
            preparedStatement.executeUpdate();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException exception) {
            exception.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public int getCount(int passProductId){
        int count = 0;
        String sqlStatement = "SELECT count FROM products WHERE id = ?";
        try(Connection connection = connection();
        PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)){
            preparedStatement.setInt(1, passProductId);
            try(ResultSet resultSet = preparedStatement.executeQuery()){
                while (resultSet.next()){
                    count = resultSet.getInt("count");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException exception) {
            exception.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return count;
    }

    public long addUser(User user){
        long userId = 0;
        String sqlStatement = "INSERT INTO users(email, password, first_name, last_name, balance, type)" +
                "VALUES (?, ?, ?, ?, ?, ?)";
        StrongPasswordEncryptor encryptor = new StrongPasswordEncryptor();
        try(Connection connection = connection();
        PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement, Statement.RETURN_GENERATED_KEYS)){
        preparedStatement.setString(1, user.getEmail());
        preparedStatement.setString(2, encryptor.encryptPassword(user.getPassword()));
        preparedStatement.setString(3, user.getFirstName());
        preparedStatement.setString(4, user.getLastName());

        if(user instanceof Customer){
            preparedStatement.setInt(5, 100);
            preparedStatement.setString(6, ((Customer) user).getType());
        }
        else if(user instanceof Admin){
            preparedStatement.setInt(5, Integer.MIN_VALUE);
            preparedStatement.setString(6, ((Admin) user).getType());
        }

        int affectedRows = preparedStatement.executeUpdate();
        if(affectedRows > 0 ){
            try(ResultSet resultSet = preparedStatement.getGeneratedKeys()){
            if(resultSet.next()){
                userId = resultSet.getLong(1);
            }
            }
        }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException exception) {
            exception.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return userId;
    }

    public void deleteUser(String passEmail){
        String sqlStatement = "DELETE FROM cart WHERE NOT EXISTS(SELECT * FROM users WHERE cart.user_cart_id = users.user_id  AND email = ?); DELETE FROM users WHERE email = ?";
        try(Connection connection = connection();
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)){
            preparedStatement.setString(1, passEmail);
            preparedStatement.setString(2, passEmail);
            preparedStatement.executeUpdate();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException exception) {
            exception.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public boolean checkUser(String passEmail, String passPassword){

        String sqlStatement = "SELECT email, password FROM users WHERE email = ?";
        boolean checked = false;
        try(Connection connection = connection();
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setString(1, passEmail);

            try(ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    String encryptedStoredPassword = resultSet.getString("password");
                    StrongPasswordEncryptor encryptor = new StrongPasswordEncryptor();
                    if(encryptor.checkPassword(passPassword, encryptedStoredPassword)){
                        checked = true;
                    }
                    else{
                        checked = false;
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException exception) {
            exception.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return checked;
    }

    public String getNameOfUser(String passEmail, String passPassword){

        String sqlStatement = "SELECT first_name, last_name, password FROM users WHERE email = ?";
        String name = "";
        try(Connection connection = connection();
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setString(1, passEmail);
            try(ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    String encryptedStoredPassword = resultSet.getString("password");
                    StrongPasswordEncryptor encryptor = new StrongPasswordEncryptor(); //to encrypt the password that user entered

                    if(encryptor.checkPassword(passPassword, encryptedStoredPassword)){
                        name = resultSet.getString("first_name");
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException exception) {
            exception.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return name;
    }
}
