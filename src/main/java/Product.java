public class Product {
    private int id;
    private String name;
    private int price;
    private int count;
    private int totalPrice;

    public Product(){}

    public Product(String name, int price, int count){
        this.name = name;
        this.price = price;
        this.count = count;
    }

    public Product(int id, String name, int price, int count){
        this(name, price, count);
        this.id = id;
    }

    public Product(int id, String name, int price, int count, int totalPrice){
        this(id, name, price, count);
        this.totalPrice = totalPrice;
    }


    public String getName() { return name; }

    public int getPrice() { return price; }

    public int getCount() { return count; }

    public void setId(int id){ this.id = id; }

    public void setName(String name){ this.name = name; }

    public void setPrice(int price){ this.price = price; }

    public void setCount(int count){ this.count = count; }

    @Override

    public String toString(){
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", count=" + count +
                ", total price=" + totalPrice +
                '}';
    }
}
